package com.himanshu.iotproject;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class AirMonitor extends AppCompatActivity {

    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    // String for MAC address
    private static String address;
    final int handlerState = 0; //Identify handler message
    ActionBar actionbar;
    TextView textTitle;
    android.app.ActionBar.LayoutParams layoutparams;
    TextView textViewRoomTemperature, textViewHumidity, textViewHeatIndex;
    TextView textViewCurrentTemperatureShortDesc, textViewHumidityShortDesc, textViewHeatIndexShortDesc, textViewRoomMessage;
    TextView textViewCO2Reading, textViewCOReading, textViewEthanolReading, textViewAcetoneReading, textViewAmmoniumReading, textViewTolueneReading;
    TextView textViewCO2ShortDesc, textViewCOShortDesc, textViewEthanolShortDesc, textViewAmmoniumShortDesc, textViewAcetoneShortDesc, textViewTolueneShortDesc;
    TextView textViewCoGasMessage, textViewCo2GasMessage, textViewAmmoniumGasMessage, textViewEthanolGasMessage, textViewAcetoneGasMessage, textViewTolueneGasMessage;
    RelativeLayout relativeLayoutRoomMessage;
    RelativeLayout relativeLayoutCoMessage;
    RelativeLayout relativeLayoutCo2Message;
    RelativeLayout relativeLayoutAmmoniumMessage;
    RelativeLayout relativeLayoutEthanolMessage;
    RelativeLayout relativeLayoutAcetoneMessage;
    RelativeLayout relativeLayoutTolueneMessage;
    Button buttonTemperatureGraph;
    Button buttonHumidityGraph;
    Button buttonHeatIndexGraph;
    Handler handlerBluetooth;
    //for clean up
    int clearHandler = 0;

    float[] carbonPPM = new float[100];
    int chartCountCarbon = 0;

    float[] monoPPM = new float[100];
    int chartCountMono = 0;

    float[] ammoniumPPM = new float[100];
    int chartCountAmmonium = 0;

    float[] ethanolPPM = new float[100];
    int chartCountEthanol = 0;

    float[] acetonePPM = new float[100];
    int chartCountAcetone = 0;

    float[] toluenePPM = new float[100];
    int chartCountToluene = 0;

    float temperature[] = new float[100];
    int chartCountTemperature = 0;

    float humidity[] = new float[100];
    int chartCountHumidity = 0;

    float roomheatIndex[] = new float[100];
    int chartCountHeatIndex = 0;
    //Firebase
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    String todayDate;
    //Forecast
    Button buttonForecast;
    CardView cardViewTemperatureForecast;
    CardView cardViewHeatIndexForecast;
    CardView cardViewCO2Forecast;
    CardView cardViewEthanolForecast;
    TextView textViewTemperatureForecast;
    TextView textViewHeatIndexForecast;
    TextView textViewCO2Forecast;
    TextView textViewEthanolForecast;
    private BluetoothAdapter bluetoothAdapter = null;
    private BluetoothSocket bluetoothSocket = null;
    private ConnectedThread mConnectedThread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_air_monitor);
        ActionBarTitleGravity();

        //get today's date
        Calendar calendar = Calendar.getInstance();

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        todayDate = simpleDateFormat.format(date);

        textViewRoomTemperature = findViewById(R.id.textViewCurrentTemperature);
        textViewHumidity = findViewById(R.id.textViewHumidity);
        textViewHeatIndex = findViewById(R.id.textViewHeatIndex);
        textViewCurrentTemperatureShortDesc = findViewById(R.id.textViewCurrentTemperatureShortDesc);
        textViewHumidityShortDesc = findViewById(R.id.textViewHumidityShortDesc);
        textViewHeatIndexShortDesc = findViewById(R.id.textViewHeatIndexShortDesc);
        textViewRoomMessage = findViewById(R.id.textViewRoomMessage);

        textViewCO2Reading = findViewById(R.id.textViewCO2Reading);
        textViewCOReading = findViewById(R.id.textViewCOReading);
        textViewAmmoniumReading = findViewById(R.id.textViewAmmoniumReading);
        textViewEthanolReading = findViewById(R.id.textViewEthanolReading);
        textViewAcetoneReading = findViewById(R.id.textViewAcetoneReading);
        textViewTolueneReading = findViewById(R.id.textViewTolueneReading);

        textViewCO2ShortDesc = findViewById(R.id.textViewCO2ShortDesc);
        textViewCOShortDesc = findViewById(R.id.textViewCOShortDesc);
        textViewEthanolShortDesc = findViewById(R.id.textViewEthanolShortDesc);
        textViewAmmoniumShortDesc = findViewById(R.id.textViewAmmoniumShortDesc);
        textViewAcetoneShortDesc = findViewById(R.id.textViewAcetoneShortDesc);
        textViewTolueneShortDesc = findViewById(R.id.textViewTolueneShortDesc);

        textViewCoGasMessage = findViewById(R.id.textViewCoGasMessage);
        textViewCo2GasMessage = findViewById(R.id.textViewCo2GasMessage);
        textViewAmmoniumGasMessage = findViewById(R.id.textViewAmmoniumGasMessage);
        textViewEthanolGasMessage = findViewById(R.id.textViewEthanolGasMessage);
        textViewAcetoneGasMessage = findViewById(R.id.textViewAcetoneGasMessage);
        textViewTolueneGasMessage = findViewById(R.id.textViewTolueneGasMessage);

        relativeLayoutRoomMessage = (RelativeLayout) findViewById(R.id.relativeLayoutRoomMessage); //Heat-Index
        relativeLayoutCoMessage = (RelativeLayout) findViewById(R.id.relativeLayoutCoGasMessage);
        relativeLayoutCo2Message = (RelativeLayout) findViewById(R.id.relativeLayoutCo2GasMessage);
        relativeLayoutAmmoniumMessage = (RelativeLayout) findViewById(R.id.relativeLayoutAmmoniumGasMessage);
        relativeLayoutEthanolMessage = (RelativeLayout) findViewById(R.id.relativeLayoutEthanolGasMessage);
        relativeLayoutAcetoneMessage = (RelativeLayout) findViewById(R.id.relativeLayoutAcetoneGasMessage);
        relativeLayoutTolueneMessage = (RelativeLayout) findViewById(R.id.relativeLayoutTolueneGasMessage);

        buttonTemperatureGraph = findViewById(R.id.buttonRoomTemperatureGraph);
        buttonHumidityGraph = findViewById(R.id.buttonHumidityGraph);
        buttonHeatIndexGraph = findViewById(R.id.buttonHeatIndexGraph);

        //Firedatabase instances and creating key based on today's date
        databaseReference = FirebaseDatabase.getInstance().getReference(); //instance of firebase db

        final DatabaseReference databaseReferenceTemperature = databaseReference.child("Temperature").child(todayDate);
        final DatabaseReference databaseReferenceHumidity = databaseReference.child("Humidity").child(todayDate);
        final DatabaseReference databaseReferenceHeatIndex = databaseReference.child("Heat Index").child(todayDate);
        final DatabaseReference databaseReferenceCarbon = databaseReference.child("Carbon Dioxide").child(todayDate);
        final DatabaseReference databaseReferenceMono = databaseReference.child("Carbon Monoxide").child(todayDate);
        final DatabaseReference databaseReferenceAmmonium = databaseReference.child("Ammonium").child(todayDate);
        final DatabaseReference databaseReferenceEthanol = databaseReference.child("Ethanol").child(todayDate);
        final DatabaseReference databaseReferenceAcetone = databaseReference.child("Acetone").child(todayDate);
        final DatabaseReference databaseReferenceToluene = databaseReference.child("Toluene").child(todayDate);

        //forecast
        buttonForecast = findViewById(R.id.buttonForecast);
        cardViewTemperatureForecast = findViewById(R.id.cardViewTemperatureForecast);
        cardViewHeatIndexForecast = findViewById(R.id.cardViewHeatIndexForecast);
        cardViewCO2Forecast = findViewById(R.id.cardViewCO2Forecast);
        cardViewEthanolForecast = findViewById(R.id.cardViewEthanolForecast);
        textViewTemperatureForecast = findViewById(R.id.textViewTemperatureForecast);
        textViewHeatIndexForecast = findViewById(R.id.textViewHeatIndexForecast);
        textViewCO2Forecast = findViewById(R.id.textViewCO2Forecast);
        textViewEthanolForecast = findViewById(R.id.textViewEthanolForecast);

        handlerBluetooth = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == handlerState) {
                    String readMessage = (String) msg.obj;

                    //temperature
                    if (readMessage.contains("p")) {

                        readMessage = readMessage.substring(1);
                        if (!readMessage.contains("p")) {
                            Float roomTemperature = Float.parseFloat(readMessage);
                            textViewRoomTemperature.setText("" + roomTemperature + " Degree Celsius");
                            if (roomTemperature >= 15 && roomTemperature <= 25) {
                                textViewCurrentTemperatureShortDesc.setTextSize(13);
                                textViewCurrentTemperatureShortDesc.setText("Room Temperature is NORMAL");
                            } else if (roomTemperature >= 25 && roomTemperature <= 30) {
                                textViewCurrentTemperatureShortDesc.setTextSize(13);
                                textViewCurrentTemperatureShortDesc.setText("Room Temperature is MODERATE");
                            } else if (roomTemperature >= 30 && roomTemperature <= 35) {
                                textViewCurrentTemperatureShortDesc.setTextSize(13);
                                textViewCurrentTemperatureShortDesc.setText("Room Temperature is QUITE HOT");
                            } else if (roomTemperature >= 35 && roomTemperature <= 43) {
                                textViewCurrentTemperatureShortDesc.setTextSize(13);
                                textViewCurrentTemperatureShortDesc.setText("Room Temperature is HOT");
                            } else {
                                textViewCurrentTemperatureShortDesc.setTextSize(13);
                                textViewCurrentTemperatureShortDesc.setTextColor(Color.RED);
                                textViewCurrentTemperatureShortDesc.setText("EXTREME ROOM TEMPERATURE");
                                int toastCount = 1;
                                if (toastCount == 1) {
                                    Toast.makeText(getApplicationContext(), "You might want to check room condition", Toast.LENGTH_SHORT).show();
                                    toastCount = 2;
                                }
                            }
                            //chart
                            if (chartCountTemperature < 100) {
                                temperature[chartCountTemperature] = roomTemperature;
                                chartCountTemperature++;
                            }
                            //firebase
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString(); //get timestamp

                            databaseReferenceTemperature.child(ts)
                                    .setValue(roomTemperature)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            int count = 1;
                                            if (task.isSuccessful()) {
                                                //Toast.makeText(getApplicationContext(), "Data Pushed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Exception e = task.getException();
                                                Log.d("FIREBASE_ERROR", e.toString());
                                            }
                                        }
                                    });

                        }

                    }

                    //Humidity
                    if (readMessage.contains("d")) {
                        readMessage = readMessage.substring(1).toString();

                        if (!readMessage.contains("d")) {
                            Float humid = Float.parseFloat(readMessage);
                            textViewHumidity.setText("" + humid + " %");
                            if (humid < 25) {
                                textViewHumidityShortDesc.setTextSize(13);
                                textViewHumidityShortDesc.setText("Might Experience Uncomfortable Dryness");
                            } else if (humid > 60) {
                                textViewHumidityShortDesc.setTextSize(13);
                                textViewHumidityShortDesc.setText("Might Experience Uncomfortable Wet");
                            } else {
                                textViewHumidityShortDesc.setTextSize(13);
                                textViewHumidityShortDesc.setText("Relative Humidity");
                            }
                            //chart
                            if (chartCountHumidity < 100) {
                                humidity[chartCountHumidity] = humid;
                                chartCountHumidity++;
                            }
                            //firebase
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString(); //get timestamp

                            databaseReferenceHumidity.child(ts)
                                    .setValue(humid)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            int count = 1;
                                            if (task.isSuccessful()) {
                                                //Toast.makeText(getApplicationContext(), "Data Pushed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Exception e = task.getException();
                                                Log.d("FIREBASE_ERROR", e.toString());
                                            }
                                        }
                                    });
                        }
                    }

                    //Heat Index
                    if (readMessage.contains("x")) {
                        readMessage = readMessage.substring(1).toString();
                        if (!readMessage.contains("x")) {
                            Float heatIndex = Float.parseFloat(readMessage);
                            textViewHeatIndex.setText("" + heatIndex + " %");
                            if (heatIndex <= 26.6) {
                                textViewHeatIndexShortDesc.setTextSize(13);
                                textViewHeatIndexShortDesc.setText("NORMAL");
                                relativeLayoutRoomMessage.setBackgroundColor(Color.parseColor("#C8E6C9"));
                                textViewRoomMessage.setText("Idle Heat Index");
                            } else if (heatIndex > 26.6 && heatIndex <= 32.2) {
                                textViewHeatIndexShortDesc.setTextSize(13);
                                textViewHeatIndexShortDesc.setText("CAUTION");
                                relativeLayoutRoomMessage.setBackgroundColor(Color.parseColor("#FFF9C4"));
                                textViewRoomMessage.setText("Fatigue possible with prolonged exposure and/or physical activity");
                            } else if (heatIndex > 32.2 && heatIndex <= 39) {
                                textViewHeatIndexShortDesc.setTextSize(13);
                                textViewHeatIndexShortDesc.setText("EXTREME CAUTION");
                                relativeLayoutRoomMessage.setBackgroundColor(Color.parseColor("#FBC02D"));
                                textViewRoomMessage.setText("Heat exhaustion possible with prolonged exposure and/or physical activity");
                            } else if (heatIndex > 39 && heatIndex <= 51.1) {
                                textViewHeatIndexShortDesc.setTextSize(13);
                                textViewHeatIndexShortDesc.setText("DANGER");
                                relativeLayoutRoomMessage.setBackgroundColor(Color.parseColor("#F57F17"));
                                textViewRoomMessage.setText("Heat exhaustion likely, and heat stroke possible with prolonged exposure and/or physical activity");
                            } else {
                                textViewHeatIndexShortDesc.setTextSize(13);
                                textViewHeatIndexShortDesc.setText("EXTREME DANGER");
                                relativeLayoutRoomMessage.setBackgroundColor(Color.parseColor("#F44336"));
                                textViewRoomMessage.setText("Heat stroke highly likely");
                            }
                            //chart
                            if (chartCountHeatIndex < 100) {
                                roomheatIndex[chartCountHeatIndex] = heatIndex;
                                chartCountHeatIndex++;
                            }
                            //firebase
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString(); //get timestamp

                            databaseReferenceHeatIndex.child(ts)
                                    .setValue(heatIndex)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            int count = 1;
                                            if (task.isSuccessful()) {
                                                //Toast.makeText(getApplicationContext(), "Data Pushed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Exception e = task.getException();
                                                Log.d("FIREBASE_ERROR", e.toString());
                                            }
                                        }
                                    });
                        } else {
                            textViewHeatIndex.setText("" + readMessage.substring(1).toString() + " %");
                        }

                    }

                    //Carbon
                    if (readMessage.contains("n")) {
                        readMessage = readMessage.substring(1).toString();
                        if (!readMessage.contains("n")) {
                            Float carbon = Float.parseFloat(readMessage);
                            textViewCO2Reading.setText("" + carbon + " PPM");
                            if (carbon < 1000) {
                                relativeLayoutCo2Message.setBackgroundColor(Color.parseColor("#C8E6C9"));
                                textViewCo2GasMessage.setText("Carbon Dioxide PPM Level is within Permissible Limits");
                            } else {
                                relativeLayoutCo2Message.setBackgroundColor(Color.parseColor("#FF5252"));
                                textViewCo2GasMessage.setText("Carbon Dioxide PPM Level exceeded Permissible Limits!");
                            }
                            //chart
                            if (chartCountCarbon < 100) {
                                carbonPPM[chartCountCarbon] = carbon;
                                chartCountCarbon++;
                            }
                            //firebase
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString(); //get timestamp

                            databaseReferenceCarbon.child(ts)
                                    .setValue(carbon)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            int count = 1;
                                            if (task.isSuccessful()) {
                                                //Toast.makeText(getApplicationContext(), "Data Pushed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Exception e = task.getException();
                                                Log.d("FIREBASE_ERROR", e.toString());
                                            }
                                        }
                                    });
                        }
                    }

                    //monoxide
                    if (readMessage.contains("o")) {
                        readMessage = readMessage.substring(1).toString();
                        if (!readMessage.contains("o")) {
                            Float mono = Float.parseFloat(readMessage);
                            textViewCOReading.setText("" + mono + " PPM");
                            if (mono < 9) {
                                relativeLayoutCoMessage.setBackgroundColor(Color.parseColor("#C8E6C9"));
                                textViewCoGasMessage.setText("Carbon Monoxide PPM Level is within Permissible Limits");
                            } else {
                                relativeLayoutCoMessage.setBackgroundColor(Color.parseColor("#FF5252"));
                                textViewCoGasMessage.setText("Carbon Monoxide PPM Level exceeded Permissible Limits!");
                            }
                            //chart
                            if (chartCountMono < 100) {
                                monoPPM[chartCountMono] = mono;
                                chartCountMono++;
                            }
                            //firebase
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString(); //get timestamp

                            databaseReferenceMono.child(ts)
                                    .setValue(mono)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            int count = 1;
                                            if (task.isSuccessful()) {
                                                //Toast.makeText(getApplicationContext(), "Data Pushed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Exception e = task.getException();
                                                Log.d("FIREBASE_ERROR", e.toString());
                                            }
                                        }
                                    });
                        }
                    }

                    //ethanol
                    if (readMessage.contains("h")) {
                        readMessage = readMessage.substring(1).toString();
                        if (!readMessage.contains("h")) {
                            Float Ethanol = Float.parseFloat(readMessage);
                            textViewEthanolReading.setText("" + Ethanol + " PPM");
                            if (Ethanol < 1000) {
                                relativeLayoutEthanolMessage.setBackgroundColor(Color.parseColor("#C8E6C9"));
                                textViewEthanolGasMessage.setText("Ethanol PPM Level is within Permissible Limits");
                            } else {
                                relativeLayoutEthanolMessage.setBackgroundColor(Color.parseColor("#FF5252"));
                                textViewEthanolGasMessage.setText("Ethanol PPM Level exceeded Permissible Limits!");
                            }
                            //chart
                            if (chartCountEthanol < 100) {
                                ethanolPPM[chartCountEthanol] = Ethanol;
                                chartCountEthanol++;
                            }
                            //firebase
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString(); //get timestamp

                            databaseReferenceEthanol.child(ts)
                                    .setValue(Ethanol)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            int count = 1;
                                            if (task.isSuccessful()) {
                                                //Toast.makeText(getApplicationContext(), "Data Pushed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Exception e = task.getException();
                                                Log.d("FIREBASE_ERROR", e.toString());
                                            }
                                        }
                                    });
                        }
                    }

                    //ammonium
                    if (readMessage.contains("m")) {
                        readMessage = readMessage.substring(1).toString();
                        if (!readMessage.contains("m")) {
                            Float Ammonium = Float.parseFloat(readMessage);
                            textViewAmmoniumReading.setText("" + Ammonium + " PPM");
                            if (Ammonium < 25) {
                                relativeLayoutAmmoniumMessage.setBackgroundColor(Color.parseColor("#C8E6C9"));
                                textViewAmmoniumGasMessage.setText("Ammonium PPM Level is within Permissible Limits");
                            } else {
                                relativeLayoutAmmoniumMessage.setBackgroundColor(Color.parseColor("#FF5252"));
                                textViewAmmoniumGasMessage.setText("Ammonium PPM Level exceeded Permissible Limits!");
                            }
                            //chart
                            if (chartCountAmmonium < 100) {
                                ammoniumPPM[chartCountAmmonium] = Ammonium;
                                chartCountAmmonium++;
                            }
                            //firebase
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString(); //get timestamp

                            databaseReferenceAmmonium.child(ts)
                                    .setValue(Ammonium)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            int count = 1;
                                            if (task.isSuccessful()) {
                                                //Toast.makeText(getApplicationContext(), "Data Pushed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Exception e = task.getException();
                                                Log.d("FIREBASE_ERROR", e.toString());
                                            }
                                        }
                                    });
                        }
                    }

                    //acetone
                    if (readMessage.contains("E")) {
                        readMessage = readMessage.substring(1).toString();
                        if (!readMessage.contains("E")) {
                            Float Acetone = Float.parseFloat(readMessage);
                            textViewAcetoneReading.setText("" + Acetone + " PPM");
                            if (Acetone < 250) {
                                relativeLayoutAcetoneMessage.setBackgroundColor(Color.parseColor("#C8E6C9"));
                                textViewAcetoneGasMessage.setText("Acetone PPM Level is within Permissible Limits");
                            } else {
                                relativeLayoutAcetoneMessage.setBackgroundColor(Color.parseColor("#FF5252"));
                                textViewAcetoneGasMessage.setText("Acetone PPM Level exceeded Permissible Limits!");
                            }
                            //chart
                            if (chartCountAcetone < 100) {
                                acetonePPM[chartCountAcetone] = Acetone;
                                chartCountAcetone++;
                            }
                            //firebase
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString(); //get timestamp

                            databaseReferenceAcetone.child(ts)
                                    .setValue(Acetone)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            int count = 1;
                                            if (task.isSuccessful()) {
                                                //Toast.makeText(getApplicationContext(), "Data Pushed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Exception e = task.getException();
                                                Log.d("FIREBASE_ERROR", e.toString());
                                            }
                                        }
                                    });
                        }
                    }

                    //toluene
                    if (readMessage.contains("L")) {
                        readMessage = readMessage.substring(1).toString();
                        if (!readMessage.contains("L")) {
                            Float Toluene = Float.parseFloat(readMessage);
                            textViewTolueneReading.setText("" + Toluene + " PPM");
                            if (Toluene < 200) {
                                relativeLayoutTolueneMessage.setBackgroundColor(Color.parseColor("#C8E6C9"));
                                textViewTolueneGasMessage.setText("Toluene PPM Level is within Permissible Limits");
                            } else {
                                relativeLayoutTolueneMessage.setBackgroundColor(Color.parseColor("#FF5252"));
                                textViewTolueneGasMessage.setText("Toluene PPM Level exceeded Permissible Limits!");
                            }
                            //chart
                            if (chartCountToluene < 100) {
                                toluenePPM[chartCountToluene] = Toluene;
                                chartCountToluene++;
                            }
                            //firebase
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString(); //get timestamp

                            databaseReferenceToluene.child(ts)
                                    .setValue(Toluene)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            int count = 1;
                                            if (task.isSuccessful()) {
                                                //Toast.makeText(getApplicationContext(), "Data Pushed", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Exception e = task.getException();
                                                Log.d("FIREBASE_ERROR", e.toString());
                                            }
                                        }
                                    });
                        }
                    }

                    if (chartCountTemperature >= 3 && chartCountHeatIndex >= 3 && chartCountCarbon >= 3 && chartCountEthanol >= 3) {
                        buttonForecast.setEnabled(true);
                    }
                }
            }
        }; //Handler ends

        //Gas short description
        textViewCO2ShortDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent carbonActivity = new Intent(AirMonitor.this, CarbonDioxide.class);
                Bundle bundle = new Bundle();
                bundle.putInt("totalValues", chartCountCarbon);
                bundle.putFloatArray("array", carbonPPM);
                carbonActivity.putExtras(bundle);
                startActivity(carbonActivity);
            }
        });

        textViewCOShortDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent monoActvity = new Intent(AirMonitor.this, CarbonMonoxide.class);
                Bundle bundle = new Bundle();
                bundle.putInt("totalValues", chartCountMono);
                bundle.putFloatArray("array", monoPPM);
                monoActvity.putExtras(bundle);
                startActivity(monoActvity);
            }
        });

        textViewAmmoniumShortDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ammoActivity = new Intent(AirMonitor.this, Ammonium.class);
                Bundle bundle = new Bundle();
                bundle.putInt("totalValues", chartCountAmmonium);
                bundle.putFloatArray("array", ammoniumPPM);
                ammoActivity.putExtras(bundle);
                startActivity(ammoActivity);
            }
        });

        textViewEthanolShortDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ethanolActivity = new Intent(AirMonitor.this, Ethanol.class);
                Bundle bundle = new Bundle();
                bundle.putInt("totalValues", chartCountEthanol);
                bundle.putFloatArray("array", ethanolPPM);
                ethanolActivity.putExtras(bundle);
                startActivity(ethanolActivity);
            }
        });

        textViewAcetoneShortDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent acetoneActivity = new Intent(AirMonitor.this, Acetone.class);
                Bundle bundle = new Bundle();
                bundle.putInt("totalValues", chartCountAcetone);
                bundle.putFloatArray("array", acetonePPM);
                acetoneActivity.putExtras(bundle);
                startActivity(acetoneActivity);
            }
        });

        textViewTolueneShortDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tolueneActivity = new Intent(AirMonitor.this, Toluene.class);
                Bundle bundle = new Bundle();
                bundle.putInt("totalValues", chartCountToluene);
                bundle.putFloatArray("array", toluenePPM);
                tolueneActivity.putExtras(bundle);
                startActivity(tolueneActivity);
            }
        });

        buttonHumidityGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent humidityActivity = new Intent(AirMonitor.this, HumidityGraph.class);
                Bundle bundle = new Bundle();
                bundle.putInt("totalValues", chartCountHumidity);
                bundle.putFloatArray("array", humidity);
                humidityActivity.putExtras(bundle);
                startActivity(humidityActivity);
            }
        });

        buttonTemperatureGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent temperatureActivity = new Intent(AirMonitor.this, TemperatureGraph.class);
                Bundle bundle = new Bundle();
                bundle.putInt("totalValues", chartCountTemperature);
                bundle.putFloatArray("array", temperature);
                temperatureActivity.putExtras(bundle);
                startActivity(temperatureActivity);
            }
        });

        buttonHeatIndexGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent heatIndexActivity = new Intent(AirMonitor.this, HeatIndexGraph.class);
                Bundle bundle = new Bundle();
                bundle.putInt("totalValues", chartCountHeatIndex);
                bundle.putFloatArray("array", roomheatIndex);
                heatIndexActivity.putExtras(bundle);
                startActivity(heatIndexActivity);
            }
        });

        //Forecast

        buttonForecast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardViewTemperatureForecast.setVisibility(View.VISIBLE);
                cardViewHeatIndexForecast.setVisibility(View.VISIBLE);
                cardViewCO2Forecast.setVisibility(View.VISIBLE);
                cardViewEthanolForecast.setVisibility(View.VISIBLE);

                float predictedTempList = getMean(temperature, chartCountTemperature);
                cardViewTemperatureForecast.setBackgroundColor(Color.parseColor("#B3E5FC"));
                textViewTemperatureForecast.setText("Forecast Room Temperature is " + predictedTempList + " Degree Celsius");

                float predictedHeatIndexList = getMean(roomheatIndex, chartCountHeatIndex);
                cardViewHeatIndexForecast.setBackgroundColor(Color.parseColor("#B3E5FC"));
                textViewHeatIndexForecast.setText("Forecast Heat Index is " + predictedHeatIndexList + "");

                float predictedCOList = getMean(carbonPPM, chartCountCarbon);
                cardViewCO2Forecast.setBackgroundColor(Color.parseColor("#B3E5FC"));
                textViewCO2Forecast.setText("Forecast Carbon Dioxide is " + predictedCOList + " PPM");

                float predictedEthanolList = getMean(ethanolPPM, chartCountEthanol);
                cardViewEthanolForecast.setBackgroundColor(Color.parseColor("#B3E5FC"));
                textViewEthanolForecast.setText("Forecast Ethanol is " + predictedEthanolList + " PPM");
            }
        });

        //Bluetooth Adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter
        checkBTState();
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        //creates secure outgoing connection with BT device using UUID
    }

    @Override
    public void onResume() {
        super.onResume();

        //Get MAC address from DeviceListActivity via intent
        Intent intent = getIntent();
        //Get the MAC address from the DeviceListActivty via EXTRA
        address = intent.getStringExtra(BluetoothDevices.EXTRA_DEVICE_ADDRESS);
        //create device and set the MAC address
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);

        try {
            bluetoothSocket = createBluetoothSocket(device);
            if (bluetoothSocket == null) {
                bluetoothSocket.close();
                super.onResume();
            }
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Socket creation failed!", Toast.LENGTH_LONG).show();
        }
        // Establish the Bluetooth socket connection.
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.connect();
            } catch (IOException e) {
                try {
                    bluetoothSocket.close();
                } catch (IOException e2) {
                    Toast.makeText(getApplicationContext(), "Cannot connect to Bluetooth. Internal Error has Occurred", Toast.LENGTH_SHORT).show();
                }
            }
            mConnectedThread = new ConnectedThread(bluetoothSocket);
            mConnectedThread.start();
            //I send a character when resuming.beginning transmission to check device is connected
            //If it is not an exception will be thrown in the write method and finish() will be called
            mConnectedThread.write("x");
        } else {
            Toast.makeText(getApplicationContext(), "Bluetooth Internal Error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            //Don't leave Bluetooth sockets open when leaving activity
            bluetoothSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Checks Bluetooth is available
    private void checkBTState() {

        if (bluetoothAdapter == null) {
            Toast.makeText(getBaseContext(), "Device does not support bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (bluetoothAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    private void ActionBarTitleGravity() {
        actionbar = getSupportActionBar();
        textTitle = new TextView(getApplicationContext());
        layoutparams = new android.app.ActionBar.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        textTitle.setLayoutParams(layoutparams);
        textTitle.setText(R.string.monitoring);
        textTitle.setTextColor(Color.WHITE);
        textTitle.setGravity(Gravity.CENTER_HORIZONTAL);
        textTitle.setTextSize(17);
        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionbar.setCustomView(textTitle);

    }

    //create new class for connect thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }


        public void run() {
            byte[] buffer = new byte[256];
            int bytes;

            // Keep looping to listen for received messages
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);            //read bytes from input buffer

                    String readMessage = new String(buffer, 0, bytes);
                    if (readMessage.length() < 1 || readMessage.length() > 8) {
                        readMessage = "";
                        bytes = 0;
                    }
                    // Send the obtained bytes to the UI Activity via handler
                    if (clearHandler > 2) {
                        handlerBluetooth.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();

                    } else {
                        clearHandler++;
                        readMessage = "";
                        bytes = 0;
                    }
                } catch (IOException e) {
                    break;
                }
            }
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via out stream
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }
    //Calculate Mean
    public static float getMean(float[] arr, int count) {
        float sum = 0;
        for (int i = 0; i < count; i++) {
            sum += arr[i];
        }
        return sum / count;

    }

    //cal mode
    public static List<Integer> mode(final int[] numbers) {
        final List<Integer> modes = new ArrayList<Integer>();
        final Map<Integer, Integer> countMap = new HashMap<Integer, Integer>();

        int max = -1;

        for (final int n : numbers) {
            int count = 0;

            if (countMap.containsKey(n)) {
                count = countMap.get(n) + 1;
            } else {
                count = 1;
            }

            countMap.put(n, count);

            if (count > max) {
                max = count;
            }
        }

        for (final Map.Entry<Integer, Integer> tuple : countMap.entrySet()) {
            if (tuple.getValue() == max) {
                modes.add(tuple.getKey());
            }
        }

        return modes;
    }

}
