package com.himanshu.iotproject;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class HumidityGraph extends AppCompatActivity {

    float humidityArray[] = new float[100];
    int humidityCount;
    TextView textViewDay;

    //For chart
    private LineChart mCharts;
    private Typeface mTf;

    private int[] mColors = new int[]{
            Color.rgb(137, 230, 81),
            Color.rgb(240, 240, 30),
            Color.rgb(89, 199, 250),
            Color.rgb(250, 104, 104)
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_humidity_graph);

        mCharts = (LineChart) findViewById(R.id.chartTemperature);
        textViewDay = findViewById(R.id.textViewDay);

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String todayDate = simpleDateFormat.format(date);

        switch (day) {
            case Calendar.SUNDAY:
                textViewDay.setText("" + todayDate + " - Sunday");
            case Calendar.MONDAY:
                textViewDay.setText("" + todayDate + " - Monday");
            case Calendar.TUESDAY:
                textViewDay.setText("" + todayDate + " - Tuesday");
            case Calendar.WEDNESDAY:
                textViewDay.setText("" + todayDate + " - Wednesday");
            case Calendar.THURSDAY:
                textViewDay.setText("" + todayDate + " - Thursday");
            case Calendar.FRIDAY:
                textViewDay.setText("" + todayDate + " - Friday");
            case Calendar.SATURDAY:
                textViewDay.setText("" + todayDate + " - Saturday");
        }

        Bundle bundle = getIntent().getExtras();
        humidityCount= bundle.getInt("totalValues");
        humidityArray= bundle.getFloatArray("array");

        mTf = Typeface.createFromAsset(getAssets(), "OpenSans-Bold.ttf");
        LineData data = getData(humidityCount, 0.3);
        data.setValueTypeface(mTf);
        setupChart(mCharts, data, mColors[2 % mColors.length]);

    }

    private void setupChart(LineChart chart, LineData data, int color) {

        ((LineDataSet) data.getDataSetByIndex(0)).setCircleColorHole(color);

        Description description = new Description();
        description.setText("In Percentage");
        // yes description text
        chart.getDescription().setEnabled(true);
        chart.setDescription(description);

        // mChart.setDrawHorizontalGrid(false);
        //
        // enable / disable grid background
        chart.setDrawGridBackground(true);
//        chart.getRenderer().getGridPaint().setGridColor(Color.WHITE & 0x70FFFFFF);

        // enable touch gestures
        chart.setTouchEnabled(true);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(true);

        chart.setBackgroundColor(color);

        // set custom chart offsets (automatic offset calculation is hereby disabled)
        chart.setViewPortOffsets(10, 0, 10, 0);

        // add data
        chart.setData(data);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setEnabled(false);

        chart.getAxisLeft().setEnabled(true);
        chart.getAxisLeft().setSpaceTop(40);
        chart.getAxisLeft().setSpaceBottom(40);
        chart.getAxisRight().setEnabled(true);

        chart.getXAxis().setEnabled(true);

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Float x = e.getX();
                Float y = e.getY();
                //Toast.makeText(getApplicationContext(), " Data point number : " + x + "", Toast.LENGTH_SHORT).show(); //shows data point
                Toast.makeText(getApplicationContext(), "Humidity was : " + y + " at Data point " + x + "", Toast.LENGTH_SHORT).show(); //shows ppm level
            }

            @Override
            public void onNothingSelected() {

            }
        });
        // animate calls invalidate()...
        chart.animateX(2500);
    }

    private LineData getData(int count, double range) {

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        for (int i = 0; i < count; i++) {
            //float val = (float) (Math.random() * range) + 3;
            yVals.add(new Entry(i, humidityArray[i]));
        }

        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "Humidity");
        // set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        set1.setLineWidth(2.75f);
        set1.setCircleRadius(5f);
        set1.setCircleHoleRadius(3.5f);
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.WHITE);
        set1.setHighLightColor(Color.RED);
        set1.setDrawValues(false);

        // create a data object with the datasets
        LineData data = new LineData(set1);

        return data;
    }

}
